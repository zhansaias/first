package crud.crud1;

import java.util.List;

public interface DaoStudent {
	   public List<Student> getAllStudents();
	   public void addStudent(Student student);
	   public void updateStudent(Student student);
	   public void deleteStudent(Student student);
	   public Student getStudent(int id);

}
