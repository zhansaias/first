package crud.crud1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.naming.spi.DirStateFactory.Result;

public class StudentDAO {
	public void insert(Student objStudent) throws ClassNotFoundException, SQLException
	{
		String sql="INSERT INTO student(name) VALUES (?);";
		Connection con=Connecxtion.connection();
		PreparedStatement ps=con.prepareStatement(sql);
		ps.setString(1, objStudent.getName());
		ps.execute();
		ps.close();
		con.close();
	}
	
	public void remove(Student objStudent) throws ClassNotFoundException, SQLException
	{
		String sql="DELETE FROM student WHERE id=?;";
		Connection con=Connecxtion.connection();
		PreparedStatement ps=con.prepareStatement(sql);
		ps.setInt(1, objStudent.getId());
		ps.execute();
		ps.close();
		con.close();
		
	}
	public void alter(Student objStudent) throws ClassNotFoundException, SQLException
	{
		String sql="UPDATE FROM student SET name=? WHERE =?;";
		Connection con=Connecxtion.connection();
		PreparedStatement ps=con.prepareStatement(sql);
		ps.setString(1, objStudent.getName());
		ps.setInt(2, objStudent.getId());
		ps.execute();
		ps.close();
		con.close();
		
	}
	
	public ArrayList<Student> listOfStudent() throws ClassNotFoundException, SQLException
	{
		ArrayList<Student> listOfStudent=new ArrayList<Student>();
		String sql="SELECT * FROM student";
		Connection con=Connecxtion.connection();
		PreparedStatement ps=con.prepareStatement(sql);
		ResultSet rs=ps.executeQuery();
		while(rs.next())
		{
			Student objStudent=new Student();
			objStudent.setId(rs.getInt("id"));
			objStudent.setName(rs.getString("name"));
		}
		
		ps.close();
		con.close();
		return listOfStudent;
		
	}
	

	

}
